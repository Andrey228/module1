﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            //something code about intput an output
        }


        public int[] SwapItems(int a, int b)
        {
            int tmp;
            tmp = a;
            a = b;
            b = tmp;
            return new int[] { a,b};
        }

        public int GetMinimumValue(int[] input)
        {
            int min;
            min = input[0];
            for (int i = 0; i < input.Length; i++)
                if (input[i] < min)
                    min = input[i];
            return min;
        }
    }
}
